## Solution explanation

Since I have to use some English terms often used in programming I think it's easier to write this document in English.
It can also serve as a proof that I can use English in everyday communication. Multiple birds with one stone. 

I've tried not to overcomplicate the solution and show some of my knowledge at the same time. I created an interface so it can be implemented for Twitter or some other data provider although the solution could be improved and some endpoints split accross multiple controllers or services. The best example would be the logic for calculating word score which is a candidate for a service. Also I've put some constants in the Result entity but if they would 
change more often it's better to move them to the database. But that's a performance and programming style issue and it's ok to leave some data constants in Entity in my opinion as long as all the data is in a logical place and easy to find.

Data is queried and stored directly in the controller. I would move that in some dedicated class or service if this was a bigger application but I don't see the need for that here. Also, I have a tendency to overengineer a solution so I'm trying to make things as simple as possible whenever I have a chance, as long as requirements are satisfied. 
It's better to extend or modify some logic in the future when you need it than to have code that is not used for years after you write it. And I have seen that on a project which caused us real problems when we wanted to modify it. Not to mention a lot of uneccessary time for a simple task.

The task was fun to solve although now that I did I see a lot of possible improvements and a better solution. Especially if you want to use JSON API spec for your API. But that's always the case when I solve some new problem.

## Setuping and running the project

In the app root folder run:

```
    composer install
    php bin/console doctrine:migrations:migrate
```

and to add github provider:

```
    php bin/console doctrine:fixtures:load
```

and to run a project

```
    symfony serve
```
I'm aware that local symfony server is not a good choice for real development.

## Consuming the API

1.  Normal request: 

    ```
    GET http://localhost:8000/score/php
    ```
        
    returns a response similar to this
    
    ```
    {
        term: "php",
        score: 3.3227049289225
    }
    ```

2.  In case you search for a term that has no positive or negative (or both) results:

    ```
    GET http://localhost:8000/score/mumbojumbo
    ```
    
    returns a response similar to this
    
    ```
    {
        term: "mumbojumbo",
        score: 0
    }
    ```

3.  In case you send more than 10 requests in a minute or get a server error some other way:
    
    
    ```
    GET http://localhost:8000/score/10_requests
    ```
    
    
    returns a response similar to this
    
    ```
    {
        term: "10_requests",
        score: -1
    }
    ```
    
    In my opinion -1 is a simple and effective way to show that something is wrong with the score. Added benefit is that 
    returned data type is still integer so you can easily handle this special case on client side. 
    Result is still stored in database so if the error was temporary some mechanism to recheck should be implemented on a real project.