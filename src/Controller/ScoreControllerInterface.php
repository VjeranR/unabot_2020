<?php


namespace App\Controller;


use App\Entity\Result;
use Symfony\Component\HttpFoundation\JsonResponse;

interface ScoreControllerInterface
{
    public function scoreAWord(string $word): JsonResponse;
    public function getResultsForWord(string $word, string $scoreWord): int;
    public function constructSearchQuery(string $word, string $scoreWord, string $API_URL): string;
    public function calculateScore(string $word, Result $newResult);
}