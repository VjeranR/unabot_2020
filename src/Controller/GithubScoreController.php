<?php


namespace App\Controller;


use App\Entity\Result;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Psr\Log\LoggerInterface;

class GithubScoreController extends AbstractController implements ScoreControllerInterface
{
    private $logger;
    private $entityManager;
    private $dataProvider;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->logger = $logger;
        $this->entityManager = $em;

        $this->dataProvider = $this->entityManager->getRepository('App\Entity\DataProvider')->findOneBy(['name' => 'github']);

        $this->logger->info("Init Github Controller");
    }

    /**
     * @Route("/score/{word}")
     *
     * @param string $word
     * @return JsonResponse
     */
    public function scoreAWord(string $word): JsonResponse
    {
        $wordPopularityScore = $this->entityManager->getRepository('App\Entity\Result')->findOneBy(['word' => $word]);

        if(!empty($wordPopularityScore)) {
            $score = $wordPopularityScore->getScore();

            $this->logger->info('returning score from database: ' . $score);
        } else {
            $newResult = new Result();

            $score = $this->calculateScore($word, $newResult);

            $newResult->setWord($word);
            $newResult->setScore($score);
            $newResult->setDataProviderId($this->dataProvider->getId());

            $this->entityManager->persist($newResult);
            $this->entityManager->flush();
        }


        return JsonResponse::create(['term' => $word, 'score' => $score]);
    }

    /**
     * Accepts word we're looking for plus "rocks" or "sucks".
     * Although it can be used with any combination of words not just "rocks" or "sucks".
     *
     * Score can be positive, zero or negative. Positive and zero scores are normal scores depending on what was returned
     * from API. -1 means error occurred.
     *
     * @param $word
     * @param $scoreWord
     *
     * @return integer
     */
    public function getResultsForWord(string $word, string $scoreWord): int
    {
        $client = HttpClient::create();
        $newResult = new Result();

        $searchQuery = $this->constructSearchQuery($word, $scoreWord, $this->dataProvider->getConnectionString());

        // Simple error handling. It could be improved and wrapped in a function in a real environment.
        try {
            $response = $client->request('GET', $searchQuery);
            $raw_score = $response->toArray(true);

            $score = $raw_score['total_count'];
        } catch (ClientExceptionInterface $e) {
            $this->logger->error("Handling error 4xx.");

            $score = $newResult::SCORING_ERROR;
        } catch (DecodingExceptionInterface $e) {
            $this->logger->error("Handling decoding problem.");

            $score = $newResult::SCORING_ERROR;
        } catch (RedirectionExceptionInterface $e) {
            $this->logger->error("Handling redirect 3xx.");

            $score = $newResult::SCORING_ERROR;
        } catch (ServerExceptionInterface $e) {
            $this->logger->error("Handling error 5xx.");

            $score = $newResult::SCORING_ERROR;
        } catch (TransportExceptionInterface $e) {
            $this->logger->error("Handling transport error.");

            $score = $newResult::SCORING_ERROR;
        } catch (\Exception $e) {
            $this->logger->error("Handling unexpected error.");

            $score = $newResult::SCORING_ERROR;
        }

        return $score;
    }

    /**
     * @param $word
     * @param $scoreWord
     * @param string $API_URL
     *
     * @return string
     */
    public function constructSearchQuery(string $word,string $scoreWord, string $API_URL): string
    {
        $searchQuery = '' . $API_URL . '?q=' . $word . ' ' . $scoreWord . '&type=issues';
        $this->logger->info('Search query: ' . $searchQuery);

        return $searchQuery;
    }

    /**
     * Wrapper function that improves code readability.
     *
     * @param string $word
     * @param Result $result
     *
     * @return integer
     */
    private function getPositiveResults(string $word, Result $result): int
    {
        return $this->getResultsForWord($word, $result::POSITIVE_SCORE_WORD);
    }

    /**
     * Wrapper function that improves code readability.
     *
     * @param string $word
     * @param Result $result
     *
     * @return integer
     */
    private function getNegativeResults(string $word, Result $result): int
    {
        return $this->getResultsForWord($word, $result::NEGATIVE_SCORE_WORD);
    }

    /**
     * Business logic to calculate score.
     *
     * @param string $word
     * @param Result $newResult
     * @return float|int
     */
    public function calculateScore(string $word, Result $newResult)
    {
        $positiveScore = $this->getPositiveResults($word, $newResult);
        $negativeScore = $this->getNegativeResults($word, $newResult);

        $this->logger->info('Positive score: ' . $positiveScore . ' | Negative score: ' . $negativeScore . ' | All results together: ' . ($positiveScore + $negativeScore));

        if ($positiveScore === $newResult::SCORING_ERROR || $negativeScore === $newResult::SCORING_ERROR) {
            $score = -1;
        } else if ($positiveScore === $newResult::NO_SCORE || $negativeScore === $newResult::NO_SCORE) {
            $score = 0;
        } else {
            $score = ($positiveScore / ($positiveScore + $negativeScore)) * $newResult::SCORING_SYSTEM;
        }

        return $score;
    }
}