<?php

namespace App\DataFixtures;

use App\Entity\DataProvider;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $provider = new DataProvider();
        $provider->setName('github');
        $provider->setConnectionString('https://api.github.com/search/issues');

        $manager->persist($provider);
        $manager->flush();
    }
}
