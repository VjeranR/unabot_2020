<?php

namespace App\Entity;

use App\Repository\ResultRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ResultRepository::class)
 */
class Result
{
    /**
     * Scoring constants.
     * They should be in config file or a database in a real app. I have left them here for simplicity.
     */
    public const POSITIVE_SCORE_WORD = 'rocks';
    public const NEGATIVE_SCORE_WORD = 'sucks';

    public const SCORING_SYSTEM = 10;
    public const SCORING_ERROR = -1;
    public const NO_SCORE = 0;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $word;

    /**
     * @ORM\Column(type="float")
     */
    private $score;

    /**
     * @ORM\Column(type="integer")
     * @ORM\ManyToOne(targetEntity="Provider", inversedBy="id")
     * @ORM\JoinColumn(name="provider_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $data_provider_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWord(): ?string
    {
        return $this->word;
    }

    public function setWord(string $word): self
    {
        $this->word = $word;

        return $this;
    }

    public function getScore(): ?float
    {
        return $this->score;
    }

    public function setScore(float $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getDataProviderId(): ?int
    {
        return $this->data_provider_id;
    }

    public function setDataProviderId($data_provider_id): void
    {
        $this->data_provider_id = $data_provider_id;
    }
}
