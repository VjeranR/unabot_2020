<?php

namespace App\Entity;

use App\Repository\DataProviderRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DataProviderRepository::class)
 */
class DataProvider
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $connection_string;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getConnectionString(): ?string
    {
        return $this->connection_string;
    }

    public function setConnectionString(string $connection_string): self
    {
        $this->connection_string = $connection_string;

        return $this;
    }
}
